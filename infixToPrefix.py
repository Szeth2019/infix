from mystack import Stack

# expr is coverted to prefix
def infixToPref(expr):

	# store precedence priority in a dictionary
	precedence = {}
	precedence["*"] = 3
	precedence["/"] = 3
	precedence["+"] = 2
	precedence["-"] = 2
	precedence[")"] = 1

	operatorStack = Stack()

	# split string up into array
	charList = expr.split()

	# create a answer array
	answer = []

	# traverse from right to left in array
	for char in reversed(charList):

		# if char is letter, add to the front of the list
		if char >= 'A' and char <= 'Z':
			answer.insert(0, char)

		# else if ), add to stack
		elif char == ')':
			operatorStack.push(char)

		# elseif char is (
		elif char == '(':
			#- while not ), pop stack and add to list
			topChar = operatorStack.pop()
			while not operatorStack.isEmpty() and topChar != ")":
				answer.insert(0, topChar)
				topChar = operatorStack.pop()

		# if char is operator
		elif char in "*/+-":
			# while not empty, if stack items have > precedence, remove from stack and add to answer string
			while (operatorStack.isEmpty() is False) and (precedence[operatorStack.peek()] > precedence[char]):
				answer.insert(0, operatorStack.pop())
			# add operator to stack
			operatorStack.push(char)

		else:
			print("This is not the correct input")
			return None

	# pop rest of stack and add to answer
	while not operatorStack.isEmpty():
		answer.insert(0, operatorStack.pop())


	answerString = " "
	return answerString.join(answer)


