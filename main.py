from infixToPrefix import infixToPref
from prefixeval import evalPrefix


def main():

	s = "A + B * C + D"
	s2 = "( A + B ) * ( C + D )"
	s3 = "A * B + C * D"
	s4 = "A + B + C + D"
	s5 = "A * B + C / D"
	s6 = "( A - B / C ) * ( A / K - L )"

	print("Testing infix to prefix conversion:")
	print(infixToPref(s))
	print(infixToPref(s2))
	print(infixToPref(s3))
	print(infixToPref(s4))
	print(infixToPref(s5))
	print(infixToPref(s6))

	print()

	print("Testing prefix evaluation:")
	print(evalPrefix("2"))


if __name__ == "__main__":
	main()