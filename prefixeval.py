from mystack import Stack

def evalPrefix(expr):

	operatorString = "*/+-"
	operandString = "1234567890"

	s = Stack()

	charList = expr.split(" ")

	for char in reversed(charList):

		if char in operandString:
			s.push(char)

		elif char in operatorString:
			op1 = int(s.pop())
			op2 = int(s.pop())
			operator = char

			newOp = doMath(op1, op2, operator)
			s.push(newOp)

		else:
			print("Incorrect input.")
			return None

	return s.pop()


def doMath(operand1, operand2, operator):

	if operator == "+":
		return operand1 + operand2

	elif operator == "-":
		return operand1 - operand2

	elif operator == "*":
		return operand1 * operand2

	elif operator == "/":
		return operand1 / operand2

	else:
		print("Incorrect input.")
		return None









